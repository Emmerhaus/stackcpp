﻿#include <iostream>
#include <stdexcept>
#include <cassert>


template <typename T>
class Stack {
    T* arr = nullptr;
    int size = 0;
    int capacity = 0;
public:
    Stack() = default;
    Stack(const Stack& rhs) : arr{ new T[rhs.size] }, size{ rhs.size }, capacity{ rhs.size } {
        for (int i = 0; i < size; ++i) {
            arr[i] = rhs.arr[i];
        }
    }

    ~Stack() {
        delete[] arr;
    }

    Stack& operator=(const Stack& rhs) {
        delete[] arr;
        arr = new T[rhs.size];
        size = rhs.size;
        capacity = rhs.size;
        for (int i = 0; i < size; ++i) {
            arr[i] = rhs.arr[i];
        }
        return *this;
    }

    bool operator==(const Stack& rhs) {
        if (size != rhs.size) {
            return false;
        }

        for (int i = 0; i < size; ++i) {
            if (arr[i] != rhs.arr[i]) {
                return false;
            }
        }
        return true;
    }

    bool operator!=(const Stack& rhs) {
        return !(*this == rhs);
    }

    int Size() {
        return size;
    }

    bool empty() {
        return size == 0;
    }
    
    T pop() {
        if (empty()) {
            throw std::out_of_range{ "Out of range in Stack<T>::pop()" };
        }
        return arr[--size];
    }

    void push(const T& val) {
        if (size == capacity) {
            T* new_arr = new T[size * 2 + 1];
            for (int i = 0; i < size; ++i) {
                new_arr[i] = arr[i];
            }
            delete[] arr;
            arr = new_arr;
            arr[size] = val;
            capacity = size * 2 + 1;
            ++size;
        }
        else {
            arr[size++] = val;
        }
    }
};


int main()
{
    {
        Stack<int> s;
        assert(s.Size() == 0);
    }
    {
        Stack<int> s;
        s.push(1);
        assert(s.Size() == 1);
    }
    {
        Stack<int> s;
        s.push(1);
        s.push(2);
        assert(s.Size() == 2);
        assert(s.pop() == 2);
        assert(s.Size() == 1);
        assert(s.pop() == 1);
        assert(s.empty());
    }
    {
        Stack<int> s;
        s.push(1);
        s.push(2);
        Stack<int> t = s;
        assert(t.Size() == 2);
        assert(t.pop() == 2);
        assert(t.Size() == 1);
        assert(t.pop() == 1);
        assert(t.empty());

        assert(s.Size() == 2);
        assert(s.pop() == 2);
        assert(s.Size() == 1);
        assert(s.pop() == 1);
        assert(s.empty());
    }

    {
        Stack<int> s;
        s.push(1);
        s.push(2);
        Stack<int> t;
        t = s;
        assert(t.Size() == 2);
        assert(t.pop() == 2);
        assert(t.Size() == 1);
        assert(t.pop() == 1);
        assert(t.empty());

        assert(s.Size() == 2);
        assert(s.pop() == 2);
        assert(s.Size() == 1);
        assert(s.pop() == 1);
        assert(s.empty());
    }

    {
        Stack<int> s;
        for (int i = 0; i < 10000; ++i) {
            s.push(i);
            assert(s.Size() == i + 1);
        }
        for (int i = 9999; i >= 0; --i) {
            assert(s.pop() == i);
            assert(s.Size() == i);
        }
    }
    {
        Stack<std::string> s;
        s.push("test");
        assert(s.pop() == "test");
    }
}
